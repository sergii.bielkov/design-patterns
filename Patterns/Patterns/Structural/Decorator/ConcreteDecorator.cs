﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patterns.Structural.Decorator
{
    public class ConcreteDecorator : AbstractDecorator
    {
        public ConcreteDecorator(IDecorated decorated) : base(decorated)
        {
        }

        public override void Do()
        {
            Console.WriteLine("From decorator start");
            Decorated.Do();
            Console.WriteLine("From decorator end");
        }
    }
}
