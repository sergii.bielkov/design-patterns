﻿namespace Patterns.Structural.Decorator
{
    public abstract class AbstractDecorator : IDecorated
    {
        protected readonly IDecorated Decorated;

        protected AbstractDecorator(IDecorated decorated)
        {
            Decorated = decorated;
        }

        public abstract void Do();
    }
}
