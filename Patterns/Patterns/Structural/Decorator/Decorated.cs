﻿using System;

namespace Patterns.Structural.Decorator
{
    public sealed class Decorated : IDecorated
    {
        public void Do()
        {
            Console.WriteLine("From decorated");
        }
    }
}
