﻿namespace Patterns.Structural.Decorator
{
    public interface IDecorated
    {
        void Do();
    }
}
